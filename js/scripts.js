$(document).ready(function() {


	/***************** Waypoints ******************/

	$('.wp1').waypoint(function() {
		$('.wp1').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.wp2').waypoint(function() {
		$('.wp2').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.wp3').waypoint(function() {
		$('.wp3').addClass('animated fadeInRight');
		$('.twp3').addClass('animated fadeInRight');
	}, {
		offset: '75%'
	});
	$('.bpsig').waypoint(function() {
		$('.bpsig').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.fc1').waypoint(function() {
		$('.fc1').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.fc2').waypoint(function() {
		$('.fc2').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.fc3').waypoint(function() {
		$('.fc3').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.spa').waypoint(function() {
		$('.spa').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.fc').waypoint(function() {
		$('.fc').addClass('animated fadeInUp');
	}, {
		offset: '75%'
	});
	$('.gum').waypoint(function() {
		$('.gum').addClass('animated fadeInLeft');
	}, {
		offset: '75%'
	});
	$('.18q1').waypoint(function() {
		$('.18q1').addClass('animated fadeInLeft');
	}, {
		offset: '75%'
	});
	$('.18q2').waypoint(function() {
		$('.18q2').addClass('animated fadeInRight');
	}, {
		offset: '75%'
	});
	$('.18q3').waypoint(function() {
		$('.18q3').addClass('animated fadeInLeft');
	}, {
		offset: '75%'
	});
	$('.18q4').waypoint(function() {
		$('.18q4').addClass('animated fadeInRight');
	}, {
		offset: '75%'
	});
	/***************** Initiate Flexslider ******************/
	$('.flexslider').flexslider({
		animation: "slide"
	});

	/***************** Initiate Fancybox ******************/

	$('.single_image').fancybox({
		padding: 4,
	});

	/***************** Tooltips ******************/
    $('[data-toggle="tooltip"]').tooltip();

	/***************** Nav Transformicon ******************/

	/* When user clicks the Icon */
	$('.nav-toggle').click(function() {
		$(this).toggleClass('active');
		$('.header-nav').toggleClass('open');
		event.preventDefault();
	});
	/* When user clicks a link */
	$('.header-nav li a').click(function() {
		$('.nav-toggle').toggleClass('active');
		$('.header-nav').toggleClass('open');

	});

	/***************** Header BG Scroll ******************/

	/***************** Smooth Scrolling ******************/

	$(function() {

		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {

				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					$('html,body').animate({
						scrollTop: target.offset().top
					}, 2000);
					return false;
				}
			}
		});

	});

});
